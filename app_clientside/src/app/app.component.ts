import { Component } from '@angular/core';

export interface Song{
  name: string;
  rank: number;
}

const SONG_DATA: Song[] = [
  {name: 'Song 1', rank: 1},
  {name: 'Song 2', rank: 2},
  {name: 'Song 3', rank: 3},
  {name: 'Song 4', rank: 4},
  {name: 'Song 5', rank: 5}
]

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent{
  title = 'Radio Show Portal';
  public userSong: string = "";

  displayedColumns: string[] = ['name','rank'];
  dataSource = SONG_DATA;
}